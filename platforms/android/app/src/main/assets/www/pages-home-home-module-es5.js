(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/charging/charging.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/charging/charging.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item lines=\"none\" class=\"ion-no-padding\">\n          <ion-button slot=\"start\" size=\"small\">\n            Ravi Sharma\n          </ion-button>\n          <ion-button slot=\"end\" (click)=\"onCancel()\">\n            <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-item>\n        <ion-row class=\"ion-no-padding\">\n          <ion-col size=\"6\" class=\"ion-no-padding\">\n            <ion-row>\n              <ion-col size=\"12\">\n                <ion-img src=\"assets/img/battery.png\"></ion-img>\n                <!-- <ion-icon style=\"font-size: 15em;\" src=\"assets/img/battery.svg\">\n                </ion-icon> -->\n\n              </ion-col>\n              <ion-col size=\"12\" style=\"padding: 16px;\">\n                <p style=\"padding: 0px; margin: 5px; font-size: 1.2em;\">Range</p>\n                <div style=\"border: 1px solid #39b54a; border-radius: 5px;\">\n                  <p style=\"font-size: 1.3em; text-align: center; padding: 0px; margin: 7px;\"><b>20˜25km</b></p>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n          <ion-col size=\"6\" class=\"ion-no-padding\">\n            <ion-row>\n              <ion-col size=\"12\">\n                  <ion-img src=\"assets/img/battery2.png\"></ion-img>\n                <!-- <ion-icon style=\"font-size: 15em;\" src=\"assets/img/battery2.svg\">\n                </ion-icon> -->\n              </ion-col>\n              <ion-col size=\"12\" style=\"padding: 16px;\">\n                <p style=\"padding: 0px; margin: 5px; font-size: 1.2em;\">Range</p>\n                <div style=\"border: 1px solid #39b54a; border-radius: 5px;\">\n                  <p style=\"font-size: 1.3em; text-align: center; padding: 0px; margin: 7px;\"><b>20˜25km</b></p>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row style=\"margin-top: 50%\">\n          <ion-col size=\"1\"></ion-col>\n          <ion-col class=\"ioncol\" size=\"10\" (click)=\"onClicked()\">\n            <p style=\"text-align: center; margin: 0px; padding: 0px;\">Stop Charging & Load</p>\n          </ion-col>\n          <ion-col size=\"1\"></ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/customer-detail/battery-load/battery-load.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/customer-detail/battery-load/battery-load.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item lines=\"none\" class=\"ion-no-padding\">\n          <ion-button slot=\"start\" size=\"small\">\n            Ravi Sharma\n          </ion-button>\n          <ion-button slot=\"end\" (click)=\"onCancel()\">\n            <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-item>\n        <!-- <ion-item lines=\"none\" class=\"ion-no-padding\">\n          <ion-icon style=\"font-size: 3em; padding-left: 4px;\" src=\"assets/img/bat1.svg\"></ion-icon>\n        </ion-item> -->\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important; margin: 5px 0px 0px 0px;\">\n          <!-- <ion-card-header>\n            <ion-card-title style=\"font-size: 2.5em; font-weight: 400;\">Load battery on vehicle</ion-card-title>\n          </ion-card-header> -->\n          <ion-card-content>\n            <ion-row>\n              <ion-col size=\"12\">\n                <div style=\"border: 1px solid #39b54a; border-radius: 5px;height: 190px;width: 250px;margin: auto;\">\n                  <ion-img style=\"width: 200px;margin: auto;display: block;\"\n                    src=\"assets/img/gif/Ecart loading_07102019.gif\"></ion-img>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important; margin: 10px 0px 0px 0px;\">\n          <ion-img style=\"width: 350px; margin: auto;\" src=\"assets/img/camera_overlay.png\"></ion-img>\n          <ion-card-content>\n            <ion-row>\n              <ion-col size=\"2\"></ion-col>\n              <ion-col class=\"ioncol\" size=\"8\" (click)=\"onClicked('ok')\">Loaded BATTERY 1</ion-col>\n              <ion-col size=\"2\"></ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.html":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.html ***!
  \********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item lines=\"none\" class=\"ion-no-padding\">\n          <ion-button slot=\"start\" size=\"small\">\n            Ravi Sharma\n          </ion-button>\n          <ion-button slot=\"end\" (click)=\"onCancel()\">\n            <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-item>\n        <ion-row class=\"ion-no-padding\">\n          <ion-col size=\"2\" class=\"ion-no-padding\">\n            <!-- <ion-button slot=\"icon-only\"> -->\n              <ion-icon (click)=\"onBattery()\" style=\"font-size: 3em; padding-left: 4px;\" src=\"assets/img/bat1.svg\"></ion-icon>\n            <!-- </ion-button> -->\n          </ion-col>\n          <ion-col size=\"10\" class=\"ion-no-padding\">\n            <ion-button color=\"tertiary\" shape=\"round\">Don't require BATTERY 2</ion-button>\n          </ion-col>\n        </ion-row>\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important;\">\n          <!-- <ion-card-header>\n            <ion-card-title style=\"font-size: 2.5em; font-weight: 400;\">Load battery on vehicle</ion-card-title>\n          </ion-card-header> -->\n          <ion-card-content class=\"ion-no-padding\">\n            <ion-row>\n              <ion-col size=\"12\">\n                <div style=\"border: 1px solid #39b54a; border-radius: 5px;height: 190px;width: 250px;margin: auto;\">\n                  <ion-img style=\"width: 200px;margin: auto;display: block;\"\n                    src=\"assets/img/gif/Ecart loading_07102019.gif\"></ion-img>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important; margin: -10px 0px 0px 0px;\">\n          <ion-img style=\"width: 350px; margin: auto;\" src=\"assets/img/camera_overlay.png\"></ion-img>\n          <ion-card-content class=\"ion-no-padding\">\n            <ion-row>\n              <ion-col size=\"2\"></ion-col>\n              <ion-col class=\"ioncol\" size=\"8\">Loaded BATTERY 2</ion-col>\n              <ion-col size=\"2\"></ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/customer-detail/customer-detail.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/customer-detail/customer-detail.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content fullscreen>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item lines=\"none\" class=\"ion-no-padding\">\n          <ion-button slot=\"start\" size=\"small\">\n            Ravi Sharma\n          </ion-button>\n          <ion-button slot=\"end\" (click)=\"onCancel()\">\n            <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-item>\n        <ion-item class=\"ion-no-padding ion-text ion-text-center\">\n          <h2>Customer Details</h2>\n        </ion-item>\n        <ion-list>\n          <!-- <ion-item> -->\n          <ion-row>\n            <ion-col size=\"4\" class=\"padding-top: 7px;\">\n              <ion-label class=\"ion-text ion-text-wrap\">Permanent Address</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-textarea class=\"inputClass\" rows=\"5\" placeholder=\"Enter full adress here...\"></ion-textarea>\n            </ion-col>\n          </ion-row>\n          <!-- </ion-item>\n          <ion-item> -->\n          <ion-row>\n            <ion-col size=\"4\" class=\"padding-top: 7px;\">\n              <ion-label class=\"ion-text ion-text-wrap\">Adhar Number *</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-input class=\"inputClass\" type=\"number\" placeholder=\"Enter adhar number here...\"></ion-input>\n            </ion-col>\n          </ion-row>\n          <!-- </ion-item>\n          <ion-item> -->\n          <ion-row>\n            <ion-col size=\"4\" class=\"padding-top: 7px;\">\n              <ion-label class=\"ion-text ion-text-wrap\">D/L Number *</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-input class=\"inputClass\" type=\"number\" placeholder=\"Enter d/l number here...\"></ion-input>\n            </ion-col>\n          </ion-row>\n          <!-- </ion-item>\n          <ion-item> -->\n          <ion-row>\n            <ion-col size=\"4\" class=\"padding-top: 7px;\">\n              <ion-label class=\"ion-text ion-text-wrap\">Vehicle Regn. Number *</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-input class=\"inputClass\" type=\"number\" placeholder=\"Enter vehicle registration number here...\">\n              </ion-input>\n            </ion-col>\n          </ion-row>\n          <!-- </ion-item> -->\n          <!-- <ion-item> -->\n          <ion-row>\n            <ion-col size=\"4\" class=\"padding-top: 7px;\">\n              <ion-label class=\"ion-text ion-text-wrap\">Vehicle Type</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-select class=\"inputClass\" value=\"eschooter\" okText=\"Okay\" cancelText=\"Dismiss\">\n                <ion-select-option value=\"eschooter\">Escooter</ion-select-option>\n                <ion-select-option value=\"ebicycle\">Ebicycle</ion-select-option>\n                <ion-select-option value=\"ecart\">Ecart</ion-select-option>\n                <ion-select-option value=\"erickshaw\">Erickshaw</ion-select-option>\n              </ion-select>\n            </ion-col>\n          </ion-row>\n          <!-- </ion-item> -->\n        </ion-list>\n        <ion-row>\n          <ion-col class=\"ion-text ion-text-center\">\n            <ion-button (click)=\"onProcess()\" shape=\"round\">Payment Process</ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/handle-error/handle-error.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/handle-error/handle-error.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item lines=\"none\" class=\"ion-no-padding\">\n          <ion-button slot=\"end\" (click)=\"onCancel()\">\n            <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-item>\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important; margin: 50px 0px 0px 0px;\">\n          <ion-card-header>\n            <ion-card-title style=\"font-size: 2em; font-weight: 400;\"><b>Sorry Ravi! </b>You are not authorized to load\n              the battery.</ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-row>\n              <ion-col size=\"12\">\n                <ion-button class=\"round\" color=\"primary\" (click)=\"onClicked('ok')\">Try Again</ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-card style=\"box-shadow: none !important; margin: 80px 0px 0px 0px;\">\n          <ion-row>\n            <ion-col size=\"3\"></ion-col>\n            <ion-col size=\"6\" class=\"ion-text ion-text-center\">\n              <ion-img style=\"width: 100px;margin: auto;display: block;\" src=\"assets/img/help.png\"></ion-img>\n            </ion-col>\n            <ion-col size=\"3\"></ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/home.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Ravi, welcome to Svich</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important;margin: 115px 0px 0px 0px;\">\n          <ion-card-header>\n            <ion-card-title style=\"font-size: 2.5em; font-weight: 400;\">I would like to</ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-row>\n              <ion-col size=\"12\">\n                <ion-button class=\"round\" color=\"primary\" (click)=\"onClicked()\">BUY</ion-button>\n              </ion-col>\n              <ion-col size=\"12\">\n                <h6><b>OR</b></h6>\n              </ion-col>\n              <ion-col size=\"12\">\n                <ion-button class=\"round\" color=\"tertiary\" (click)=\"onClicked()\">RENT</ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/ride-closed/ride-closed.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/ride-closed/ride-closed.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item lines=\"none\" class=\"ion-no-padding\">\n          <ion-button slot=\"start\" size=\"small\">\n            Ravi Sharma\n          </ion-button>\n          <ion-button slot=\"end\" (click)=\"onCancel()\">\n            <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-item>\n        <ion-row class=\"ion-no-padding\">\n          <ion-col size=\"2\" class=\"ion-no-padding\">\n            <ion-icon style=\"font-size: 3em;\" src=\"assets/img/battery2.svg\">\n            </ion-icon>\n          </ion-col>\n          <ion-col size=\"2\" class=\"ion-no-padding\">\n            <ion-icon style=\"font-size: 3em; margin-left: -21px; margin-top: -10px;\" src=\"assets/img/bat1.svg\">\n            </ion-icon>\n          </ion-col>\n          <ion-col size=\"8\" class=\"ion-no-padding\"></ion-col>\n        </ion-row>\n        <ion-item class=\"ion-no-padding ion-text ion-text-center\">\n          <h2>Ride Statistics</h2>\n        </ion-item>\n        <ion-list>\n\n          <ion-row>\n            <ion-col size=\"4\" class=\"padding-top: 7px;\">\n              <ion-label class=\"ion-text ion-text-wrap\">Distance Coverd</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-input class=\"inputClass\" type=\"number\" placeholder=\"in kms...\"></ion-input>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"4\" class=\"padding-top: 7px;\">\n              <ion-label class=\"ion-text ion-text-wrap\">Time Rent</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-input class=\"inputClass\" type=\"number\" placeholder=\"in hrs...\"></ion-input>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col size=\"4\" class=\"padding-top: 7px;\">\n              <ion-label class=\"ion-text ion-text-wrap\">Unit Consumed</ion-label>\n            </ion-col>\n            <ion-col size=\"8\">\n              <ion-input class=\"inputClass\" type=\"number\" placeholder=\"in kwh...\">\n              </ion-input>\n            </ion-col>\n          </ion-row>\n        </ion-list>\n\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important; margin: 5px 0px 0px 0px;\">\n          <ion-card-header>\n            <ion-card-title>CONNECT CHARGER TO CLOSE THIS RIDE</ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-row>\n              <ion-col size=\"12\">\n                <div style=\"border: 1px solid #39b54a; border-radius: 5px;height: 190px;width: 250px;margin: auto;\">\n                  <ion-img style=\"width: 200px;margin: auto;display: block;\"\n                    src=\"assets/img/gif/Ecart loading_07102019.gif\"></ion-img>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-row>\n          <ion-col class=\"ioncol\" size=\"12\" (click)=\"onSuccess()\">BATTERY 1 CONNECTED TO CHARGER</ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/ride-start/ride-start.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/ride-start/ride-start.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-item lines=\"none\" class=\"ion-no-padding\">\n          <ion-button slot=\"start\" size=\"small\">\n            Ravi Sharma\n          </ion-button>\n          <ion-button slot=\"end\" (click)=\"onCancel()\">\n            <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n          </ion-button>\n        </ion-item>\n        <ion-row class=\"ion-no-padding\">\n          <ion-col size=\"2\" class=\"ion-no-padding\">\n            <ion-icon (click)=\"onBattery()\" style=\"font-size: 3em; padding-left: 4px;\" src=\"assets/img/battery2.svg\">\n            </ion-icon>\n          </ion-col>\n          <ion-col size=\"6\" class=\"ion-no-padding\">\n            <ion-icon (click)=\"onBattery()\" style=\"font-size: 22em;margin-left: -105px;\" src=\"assets/img/battery.svg\">\n            </ion-icon>\n          </ion-col>\n          <ion-col size=\"4\" class=\"ion-no-padding\" style=\"margin-top: 59px; margin-left: -16px;\">\n           <ion-row>\n             <ion-col size=\"12\">\n                <ion-img style=\"width: 110px; margin: auto;\" src=\"assets/img/lock.png\"></ion-img>\n             </ion-col>\n             <ion-col size=\"12\" style=\"margin-top: 58px; margin-left: -23px;\">\n                <p style=\"text-align: right;\">Range</p>\n                <div style=\"border: 1px solid #39b54a; border-radius: 5px;height: 50px;width: 165px;margin: auto;\">\n                  <p style=\"font-size: 1.8em; margin-top: 6px; margin-left: 22px;\"><b>20˜25km</b></p>\n                </div>\n             </ion-col>\n           </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important; margin: 60px 0px 0px 0px;\">\n          <ion-card-content class=\"ion-no-padding\">\n            <ion-row>\n              <ion-col size=\"12\">\n                  <ion-button (click)=\"onRideClose()\" shape=\"round\">Ride Close</ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important; margin: 60px 0px 0px 0px;\">\n          <ion-img style=\"width: 110px; margin: auto;\" src=\"assets/img/help.png\"></ion-img>\n          \n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/thank-you/thank-you.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/thank-you/thank-you.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <ion-card class=\"ion-text-center\" style=\"box-shadow: none !important; margin: 50px 0px 0px 0px;\">\n          <ion-card-header>\n            <ion-card-title style=\"font-size: 2em; font-weight: 400;\"><b>Thank You </b>Ravi</ion-card-title>\n          </ion-card-header>\n        </ion-card>\n        <ion-card style=\"box-shadow: none !important; margin: 80px 0px 0px 0px;\">\n          <ion-row>\n            <ion-col size=\"3\"></ion-col>\n            <ion-col size=\"6\" class=\"ion-text ion-text-center\">\n              <ion-button shape=\"round\" color=\"tertiary\" (click)=\"onCancel()\">CLOSE</ion-button>\n            </ion-col>\n            <ion-col size=\"3\"></ion-col>\n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/home/charging/charging.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/home/charging/charging.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ioncol {\n  border-radius: 15px;\n  border: 5px solid green;\n  font-size: 1.5em;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvc3ZpY2gtZW5kdXNlci9zcmMvYXBwL3BhZ2VzL2hvbWUvY2hhcmdpbmcvY2hhcmdpbmcuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvY2hhcmdpbmcvY2hhcmdpbmcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS9jaGFyZ2luZy9jaGFyZ2luZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pb25jb2wge1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XG4gICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xufSIsIi5pb25jb2wge1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBib3JkZXI6IDVweCBzb2xpZCBncmVlbjtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/home/charging/charging.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/home/charging/charging.component.ts ***!
  \***********************************************************/
/*! exports provided: ChargingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChargingComponent", function() { return ChargingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../thank-you/thank-you.component */ "./src/app/pages/home/thank-you/thank-you.component.ts");




var ChargingComponent = /** @class */ (function () {
    function ChargingComponent(modalController) {
        this.modalController = modalController;
    }
    ChargingComponent.prototype.ngOnInit = function () { };
    ChargingComponent.prototype.onCancel = function () {
        this.modalController.dismiss();
    };
    ChargingComponent.prototype.onClicked = function () {
        this.modalController.create({
            component: _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_3__["ThankYouComponent"]
        }).then(function (modalEl) {
            modalEl.present();
        });
    };
    ChargingComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    ChargingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'charging',
            template: __webpack_require__(/*! raw-loader!./charging.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/charging/charging.component.html"),
            styles: [__webpack_require__(/*! ./charging.component.scss */ "./src/app/pages/home/charging/charging.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], ChargingComponent);
    return ChargingComponent;
}());



/***/ }),

/***/ "./src/app/pages/home/customer-detail/battery-load/battery-load.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/home/customer-detail/battery-load/battery-load.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ioncol {\n  border-radius: 15px;\n  border: 5px solid green;\n  font-size: 1.5em;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvc3ZpY2gtZW5kdXNlci9zcmMvYXBwL3BhZ2VzL2hvbWUvY3VzdG9tZXItZGV0YWlsL2JhdHRlcnktbG9hZC9iYXR0ZXJ5LWxvYWQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvY3VzdG9tZXItZGV0YWlsL2JhdHRlcnktbG9hZC9iYXR0ZXJ5LWxvYWQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS9jdXN0b21lci1kZXRhaWwvYmF0dGVyeS1sb2FkL2JhdHRlcnktbG9hZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pb25jb2wge1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XG4gICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xufSIsIi5pb25jb2wge1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBib3JkZXI6IDVweCBzb2xpZCBncmVlbjtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/home/customer-detail/battery-load/battery-load.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/home/customer-detail/battery-load/battery-load.component.ts ***!
  \***********************************************************************************/
/*! exports provided: BatteryLoadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BatteryLoadComponent", function() { return BatteryLoadComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _load_battery_vehicle_load_battery_vehicle_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./load-battery-vehicle/load-battery-vehicle.component */ "./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.ts");
/* harmony import */ var _handle_error_handle_error_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../handle-error/handle-error.component */ "./src/app/pages/home/handle-error/handle-error.component.ts");





var BatteryLoadComponent = /** @class */ (function () {
    function BatteryLoadComponent(modalController) {
        this.modalController = modalController;
    }
    BatteryLoadComponent.prototype.ngOnInit = function () {
        // this.onClicked();
    };
    BatteryLoadComponent.prototype.onCancel = function () {
        this.modalController.dismiss();
    };
    BatteryLoadComponent.prototype.onClicked = function (key) {
        if (key === 'ok') {
            this.modalController.create({
                component: _load_battery_vehicle_load_battery_vehicle_component__WEBPACK_IMPORTED_MODULE_3__["LoadBatteryVehicleComponent"]
            }).then(function (modalEl) {
                modalEl.present();
            });
        }
        else {
            this.modalController.create({
                component: _handle_error_handle_error_component__WEBPACK_IMPORTED_MODULE_4__["HandleErrorComponent"]
            }).then(function (modalEl) {
                modalEl.present();
            });
        }
    };
    // showCamera() {
    //   (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
    // }
    // hideCamera() {
    //   (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
    //   let content = <HTMLElement>document.getElementsByTagName("body")[0];
    //   content.style.background = "white !important";
    // }
    BatteryLoadComponent.prototype.ionViewDidEnter = function () {
        // this.qrScanner.prepare()
        //   .then((status: QRScannerStatus) => {
        //     if (status.authorized) {
        //       // camera permission was granted
        //       this.toastCtrl.create({
        //         message: 'camera permission granted',
        //         duration: 1000
        //       }).then(toatEl => {
        //         toatEl.present();
        //       });
        //       // start scanning
        //       this.qrScanner.show()
        //       this.showCamera();
        //       // window.document.querySelector('ion-app').classList.add('cameraView');
        //       let scanSub = this.qrScanner.scan().subscribe((text: string) => {
        //         console.log('Scanned something', text);
        //         this.qrScanner.hide(); // hide camera preview
        //         this.hideCamera();
        //         // window.document.querySelector('ion-app').classList.remove('cameraView');
        //         this.toastCtrl.create({
        //           message: 'You scanned text is this :' + text,
        //           duration: 6000
        //         }).then(toatEl => {
        //           toatEl.present();
        //         });
        //         scanSub.unsubscribe(); // stop scanning
        //       });
        //     } else if (status.denied) {
        //       this.toastCtrl.create({
        //         message: 'camera permission was denied',
        //         duration: 3000
        //       }).then(toatEl => {
        //         toatEl.present();
        //       });
        //       // camera permission was permanently denied
        //       // you must use QRScanner.openSettings() method to guide the user to the settings page
        //       // then they can grant the permission from there
        //     } else {
        //       this.toastCtrl.create({
        //         message: 'You can ask for permission again at a later time.',
        //         duration: 3000
        //       }).then(toatEl => {
        //         toatEl.present();
        //       });
        //       // permission was denied, but not permanently. You can ask for permission again at a later time.
        //     }
        //   })
        //   .catch((e: any) => console.log('Error is', e));
    };
    // showCamera() {
    //   (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
    // }
    // hideCamera() {
    //   (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
    // }
    BatteryLoadComponent.prototype.ionViewWillLeave = function () {
        // window.document.querySelector('ion-app').classList.remove('cameraView');
    };
    BatteryLoadComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    BatteryLoadComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'battery-load',
            template: __webpack_require__(/*! raw-loader!./battery-load.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/customer-detail/battery-load/battery-load.component.html"),
            styles: [__webpack_require__(/*! ./battery-load.component.scss */ "./src/app/pages/home/customer-detail/battery-load/battery-load.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], BatteryLoadComponent);
    return BatteryLoadComponent;
}());



/***/ }),

/***/ "./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.scss":
/*!******************************************************************************************************************!*\
  !*** ./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.scss ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ioncol {\n  border-radius: 15px;\n  border: 5px solid green;\n  font-size: 1.5em;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvc3ZpY2gtZW5kdXNlci9zcmMvYXBwL3BhZ2VzL2hvbWUvY3VzdG9tZXItZGV0YWlsL2JhdHRlcnktbG9hZC9sb2FkLWJhdHRlcnktdmVoaWNsZS9sb2FkLWJhdHRlcnktdmVoaWNsZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvaG9tZS9jdXN0b21lci1kZXRhaWwvYmF0dGVyeS1sb2FkL2xvYWQtYmF0dGVyeS12ZWhpY2xlL2xvYWQtYmF0dGVyeS12ZWhpY2xlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvY3VzdG9tZXItZGV0YWlsL2JhdHRlcnktbG9hZC9sb2FkLWJhdHRlcnktdmVoaWNsZS9sb2FkLWJhdHRlcnktdmVoaWNsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pb25jb2wge1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XG4gICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xufSIsIi5pb25jb2wge1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBib3JkZXI6IDVweCBzb2xpZCBncmVlbjtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.ts ***!
  \****************************************************************************************************************/
/*! exports provided: LoadBatteryVehicleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadBatteryVehicleComponent", function() { return LoadBatteryVehicleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ride_start_ride_start_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../ride-start/ride-start.component */ "./src/app/pages/home/ride-start/ride-start.component.ts");




var LoadBatteryVehicleComponent = /** @class */ (function () {
    function LoadBatteryVehicleComponent(modalController) {
        this.modalController = modalController;
    }
    LoadBatteryVehicleComponent.prototype.ngOnInit = function () { };
    LoadBatteryVehicleComponent.prototype.onCancel = function () {
        this.modalController.dismiss();
    };
    LoadBatteryVehicleComponent.prototype.onBattery = function () {
        this.modalController.create({
            component: _ride_start_ride_start_component__WEBPACK_IMPORTED_MODULE_3__["RideStartComponent"]
        }).then(function (modalEL) {
            modalEL.present();
        });
    };
    LoadBatteryVehicleComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    LoadBatteryVehicleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'load-battery-vehicle',
            template: __webpack_require__(/*! raw-loader!./load-battery-vehicle.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.html"),
            styles: [__webpack_require__(/*! ./load-battery-vehicle.component.scss */ "./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], LoadBatteryVehicleComponent);
    return LoadBatteryVehicleComponent;
}());



/***/ }),

/***/ "./src/app/pages/home/customer-detail/customer-detail.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/home/customer-detail/customer-detail.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-item .sc-ion-textarea-md-h {\n  --padding-top: 0px;\n}\n\n.inputClass {\n  border: 1px solid #39b54a;\n  border-radius: 5px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvc3ZpY2gtZW5kdXNlci9zcmMvYXBwL3BhZ2VzL2hvbWUvY3VzdG9tZXItZGV0YWlsL2N1c3RvbWVyLWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvaG9tZS9jdXN0b21lci1kZXRhaWwvY3VzdG9tZXItZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7QUNDRjs7QURFQTtFQUNJLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL2N1c3RvbWVyLWRldGFpbC9jdXN0b21lci1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taXRlbSAuc2MtaW9uLXRleHRhcmVhLW1kLWgge1xuICAtLXBhZGRpbmctdG9wOiAwcHg7XG59XG5cbi5pbnB1dENsYXNzIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzliNTRhO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbiIsImlvbi1pdGVtIC5zYy1pb24tdGV4dGFyZWEtbWQtaCB7XG4gIC0tcGFkZGluZy10b3A6IDBweDtcbn1cblxuLmlucHV0Q2xhc3Mge1xuICBib3JkZXI6IDFweCBzb2xpZCAjMzliNTRhO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHdpZHRoOiAxMDAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/home/customer-detail/customer-detail.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/home/customer-detail/customer-detail.component.ts ***!
  \*************************************************************************/
/*! exports provided: CustomerDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerDetailComponent", function() { return CustomerDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _battery_load_battery_load_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./battery-load/battery-load.component */ "./src/app/pages/home/customer-detail/battery-load/battery-load.component.ts");




var CustomerDetailComponent = /** @class */ (function () {
    function CustomerDetailComponent(modalController) {
        this.modalController = modalController;
    }
    CustomerDetailComponent.prototype.ngOnInit = function () { };
    CustomerDetailComponent.prototype.onCancel = function () {
        this.modalController.dismiss();
    };
    CustomerDetailComponent.prototype.onProcess = function () {
        this.modalController.create({
            component: _battery_load_battery_load_component__WEBPACK_IMPORTED_MODULE_3__["BatteryLoadComponent"]
        }).then(function (modalEl) {
            modalEl.present();
        });
    };
    CustomerDetailComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    CustomerDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'customer-detail',
            template: __webpack_require__(/*! raw-loader!./customer-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/customer-detail/customer-detail.component.html"),
            styles: [__webpack_require__(/*! ./customer-detail.component.scss */ "./src/app/pages/home/customer-detail/customer-detail.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], CustomerDetailComponent);
    return CustomerDetailComponent;
}());



/***/ }),

/***/ "./src/app/pages/home/handle-error/handle-error.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/home/handle-error/handle-error.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".round {\n  width: 150px;\n  height: 60px;\n  border-radius: 5px;\n  font-size: 1.5em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvc3ZpY2gtZW5kdXNlci9zcmMvYXBwL3BhZ2VzL2hvbWUvaGFuZGxlLWVycm9yL2hhbmRsZS1lcnJvci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvaG9tZS9oYW5kbGUtZXJyb3IvaGFuZGxlLWVycm9yLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL2hhbmRsZS1lcnJvci9oYW5kbGUtZXJyb3IuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm91bmQge1xuICAgIHdpZHRoOiAxNTBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGZvbnQtc2l6ZTogMS41ZW07XG4gIH0iLCIucm91bmQge1xuICB3aWR0aDogMTUwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBmb250LXNpemU6IDEuNWVtO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/home/handle-error/handle-error.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/home/handle-error/handle-error.component.ts ***!
  \*******************************************************************/
/*! exports provided: HandleErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HandleErrorComponent", function() { return HandleErrorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var HandleErrorComponent = /** @class */ (function () {
    function HandleErrorComponent(modalController) {
        this.modalController = modalController;
    }
    HandleErrorComponent.prototype.ngOnInit = function () { };
    HandleErrorComponent.prototype.onCancel = function () {
        this.modalController.dismiss();
    };
    HandleErrorComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    HandleErrorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'handle-error',
            template: __webpack_require__(/*! raw-loader!./handle-error.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/handle-error/handle-error.component.html"),
            styles: [__webpack_require__(/*! ./handle-error.component.scss */ "./src/app/pages/home/handle-error/handle-error.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], HandleErrorComponent);
    return HandleErrorComponent;
}());



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _customer_detail_customer_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./customer-detail/customer-detail.component */ "./src/app/pages/home/customer-detail/customer-detail.component.ts");
/* harmony import */ var _customer_detail_battery_load_battery_load_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./customer-detail/battery-load/battery-load.component */ "./src/app/pages/home/customer-detail/battery-load/battery-load.component.ts");
/* harmony import */ var _customer_detail_battery_load_load_battery_vehicle_load_battery_vehicle_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component */ "./src/app/pages/home/customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component.ts");
/* harmony import */ var _handle_error_handle_error_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./handle-error/handle-error.component */ "./src/app/pages/home/handle-error/handle-error.component.ts");
/* harmony import */ var _ride_start_ride_start_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ride-start/ride-start.component */ "./src/app/pages/home/ride-start/ride-start.component.ts");
/* harmony import */ var _ride_closed_ride_closed_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ride-closed/ride-closed.component */ "./src/app/pages/home/ride-closed/ride-closed.component.ts");
/* harmony import */ var _charging_charging_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./charging/charging.component */ "./src/app/pages/home/charging/charging.component.ts");
/* harmony import */ var _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./thank-you/thank-you.component */ "./src/app/pages/home/thank-you/thank-you.component.ts");















var routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
    }
];
var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            entryComponents: [
                _customer_detail_customer_detail_component__WEBPACK_IMPORTED_MODULE_7__["CustomerDetailComponent"],
                _customer_detail_battery_load_battery_load_component__WEBPACK_IMPORTED_MODULE_8__["BatteryLoadComponent"],
                _customer_detail_battery_load_load_battery_vehicle_load_battery_vehicle_component__WEBPACK_IMPORTED_MODULE_9__["LoadBatteryVehicleComponent"],
                _handle_error_handle_error_component__WEBPACK_IMPORTED_MODULE_10__["HandleErrorComponent"],
                _ride_start_ride_start_component__WEBPACK_IMPORTED_MODULE_11__["RideStartComponent"],
                _ride_closed_ride_closed_component__WEBPACK_IMPORTED_MODULE_12__["RideClosedComponent"],
                _charging_charging_component__WEBPACK_IMPORTED_MODULE_13__["ChargingComponent"],
                _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_14__["ThankYouComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [
                _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"],
                _customer_detail_customer_detail_component__WEBPACK_IMPORTED_MODULE_7__["CustomerDetailComponent"],
                _customer_detail_battery_load_battery_load_component__WEBPACK_IMPORTED_MODULE_8__["BatteryLoadComponent"],
                _customer_detail_battery_load_load_battery_vehicle_load_battery_vehicle_component__WEBPACK_IMPORTED_MODULE_9__["LoadBatteryVehicleComponent"],
                _handle_error_handle_error_component__WEBPACK_IMPORTED_MODULE_10__["HandleErrorComponent"],
                _ride_start_ride_start_component__WEBPACK_IMPORTED_MODULE_11__["RideStartComponent"],
                _ride_closed_ride_closed_component__WEBPACK_IMPORTED_MODULE_12__["RideClosedComponent"],
                _charging_charging_component__WEBPACK_IMPORTED_MODULE_13__["ChargingComponent"],
                _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_14__["ThankYouComponent"]
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".round {\n  width: 150px;\n  height: 60px;\n  border-radius: 5px;\n  font-size: 2em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvc3ZpY2gtZW5kdXNlci9zcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFHRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQ0RGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yb3VuZCB7XG4gIC8vICAgLS1ib3JkZXItd2lkdGg6IDE1MHB4O1xuICAvLyAgIC0td2lkdGg6IDE4MHB4O1xuICB3aWR0aDogMTUwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBmb250LXNpemU6IDJlbTtcbiAgLy8gICAtLWhlaWdodDogMTgwcHg7XG4gIC8vICAgLS1ib3JkZXItcmFkaXVzOiAyNSU7XG4gIC8vICAgLS12ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAvLyAgIC0tcGFkZGluZy1zdGFydDogMTBweDtcbiAgLy8gICAtLXBhZGRpbmctZW5kOiAxMHB4O1xufVxuIiwiLnJvdW5kIHtcbiAgd2lkdGg6IDE1MHB4O1xuICBoZWlnaHQ6IDYwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZm9udC1zaXplOiAyZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _customer_detail_customer_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer-detail/customer-detail.component */ "./src/app/pages/home/customer-detail/customer-detail.component.ts");




var HomePage = /** @class */ (function () {
    function HomePage(modalController) {
        this.modalController = modalController;
    }
    HomePage.prototype.ngOnInit = function () {
    };
    HomePage.prototype.onClicked = function () {
        this.modalController.create({
            component: _customer_detail_customer_detail_component__WEBPACK_IMPORTED_MODULE_3__["CustomerDetailComponent"]
        }).then(function (modalEl) {
            modalEl.present();
        });
    };
    HomePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'home',
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], HomePage);
    return HomePage;
}());



/***/ }),

/***/ "./src/app/pages/home/ride-closed/ride-closed.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/home/ride-closed/ride-closed.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-item .sc-ion-textarea-md-h {\n  --padding-top: 0px;\n}\n\n.inputClass {\n  border: 1px solid #39b54a;\n  border-radius: 5px;\n  width: 100%;\n}\n\n.ioncol {\n  border-radius: 15px;\n  border: 5px solid green;\n  font-size: 1.5em;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcHBsZS9EZXNrdG9wL29uZXFsaWstcHJvamVjdHMvc3ZpY2gtZW5kdXNlci9zcmMvYXBwL3BhZ2VzL2hvbWUvcmlkZS1jbG9zZWQvcmlkZS1jbG9zZWQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvcmlkZS1jbG9zZWQvcmlkZS1jbG9zZWQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtBQ0NKOztBREVFO0VBQ0kseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUNDTjs7QURFRTtFQUNFLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL3JpZGUtY2xvc2VkL3JpZGUtY2xvc2VkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWl0ZW0gLnNjLWlvbi10ZXh0YXJlYS1tZC1oIHtcbiAgICAtLXBhZGRpbmctdG9wOiAwcHg7XG4gIH1cbiAgXG4gIC5pbnB1dENsYXNzIHtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMzOWI1NGE7XG4gICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5pb25jb2wge1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XG4gICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xufSIsImlvbi1pdGVtIC5zYy1pb24tdGV4dGFyZWEtbWQtaCB7XG4gIC0tcGFkZGluZy10b3A6IDBweDtcbn1cblxuLmlucHV0Q2xhc3Mge1xuICBib3JkZXI6IDFweCBzb2xpZCAjMzliNTRhO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uaW9uY29sIHtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYm9yZGVyOiA1cHggc29saWQgZ3JlZW47XG4gIGZvbnQtc2l6ZTogMS41ZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/home/ride-closed/ride-closed.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/home/ride-closed/ride-closed.component.ts ***!
  \*****************************************************************/
/*! exports provided: RideClosedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RideClosedComponent", function() { return RideClosedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _charging_charging_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../charging/charging.component */ "./src/app/pages/home/charging/charging.component.ts");




var RideClosedComponent = /** @class */ (function () {
    function RideClosedComponent(modalController) {
        this.modalController = modalController;
    }
    RideClosedComponent.prototype.ngOnInit = function () { };
    RideClosedComponent.prototype.onCancel = function () {
        this.modalController.dismiss();
    };
    RideClosedComponent.prototype.onSuccess = function () {
        this.modalController.create({
            component: _charging_charging_component__WEBPACK_IMPORTED_MODULE_3__["ChargingComponent"]
        }).then(function (modalEl) {
            modalEl.present();
        });
    };
    RideClosedComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    RideClosedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ride-closed',
            template: __webpack_require__(/*! raw-loader!./ride-closed.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/ride-closed/ride-closed.component.html"),
            styles: [__webpack_require__(/*! ./ride-closed.component.scss */ "./src/app/pages/home/ride-closed/ride-closed.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], RideClosedComponent);
    return RideClosedComponent;
}());



/***/ }),

/***/ "./src/app/pages/home/ride-start/ride-start.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/home/ride-start/ride-start.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvcmlkZS1zdGFydC9yaWRlLXN0YXJ0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/home/ride-start/ride-start.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/home/ride-start/ride-start.component.ts ***!
  \***************************************************************/
/*! exports provided: RideStartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RideStartComponent", function() { return RideStartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ride_closed_ride_closed_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ride-closed/ride-closed.component */ "./src/app/pages/home/ride-closed/ride-closed.component.ts");




var RideStartComponent = /** @class */ (function () {
    function RideStartComponent(modalController) {
        this.modalController = modalController;
    }
    RideStartComponent.prototype.ngOnInit = function () { };
    RideStartComponent.prototype.onCancel = function () {
        this.modalController.dismiss();
    };
    RideStartComponent.prototype.onRideClose = function () {
        this.modalController.create({
            component: _ride_closed_ride_closed_component__WEBPACK_IMPORTED_MODULE_3__["RideClosedComponent"]
        }).then(function (modalEl) {
            modalEl.present();
        });
    };
    RideStartComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    RideStartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ride-start',
            template: __webpack_require__(/*! raw-loader!./ride-start.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/ride-start/ride-start.component.html"),
            styles: [__webpack_require__(/*! ./ride-start.component.scss */ "./src/app/pages/home/ride-start/ride-start.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], RideStartComponent);
    return RideStartComponent;
}());



/***/ }),

/***/ "./src/app/pages/home/thank-you/thank-you.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/home/thank-you/thank-you.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvdGhhbmsteW91L3RoYW5rLXlvdS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/home/thank-you/thank-you.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/home/thank-you/thank-you.component.ts ***!
  \*************************************************************/
/*! exports provided: ThankYouComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThankYouComponent", function() { return ThankYouComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ThankYouComponent = /** @class */ (function () {
    function ThankYouComponent(modalController) {
        this.modalController = modalController;
    }
    ThankYouComponent.prototype.ngOnInit = function () { };
    ThankYouComponent.prototype.onCancel = function () {
        this.modalController.dismiss();
    };
    ThankYouComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    ThankYouComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'thank-you',
            template: __webpack_require__(/*! raw-loader!./thank-you.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/thank-you/thank-you.component.html"),
            styles: [__webpack_require__(/*! ./thank-you.component.scss */ "./src/app/pages/home/thank-you/thank-you.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], ThankYouComponent);
    return ThankYouComponent;
}());



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es5.js.map