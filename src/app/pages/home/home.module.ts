import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { BatteryLoadComponent } from './customer-detail/battery-load/battery-load.component';
import { LoadBatteryVehicleComponent } from './customer-detail/battery-load/load-battery-vehicle/load-battery-vehicle.component';
import { HandleErrorComponent } from './handle-error/handle-error.component';
import { RideStartComponent } from './ride-start/ride-start.component';
import { RideClosedComponent } from './ride-closed/ride-closed.component';
import { ChargingComponent } from './charging/charging.component';
import { ThankYouComponent } from './thank-you/thank-you.component';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  entryComponents: [
    CustomerDetailComponent, 
    BatteryLoadComponent, 
    LoadBatteryVehicleComponent,
    HandleErrorComponent,
    RideStartComponent,
    RideClosedComponent,
    ChargingComponent,
    ThankYouComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    HomePage, 
    CustomerDetailComponent, 
    BatteryLoadComponent, 
    LoadBatteryVehicleComponent,
    HandleErrorComponent,
    RideStartComponent,
    RideClosedComponent,
    ChargingComponent,
    ThankYouComponent
  ],
})
export class HomePageModule { }
