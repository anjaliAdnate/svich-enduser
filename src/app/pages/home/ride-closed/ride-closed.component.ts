import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ChargingComponent } from '../charging/charging.component';

@Component({
  selector: 'ride-closed',
  templateUrl: './ride-closed.component.html',
  styleUrls: ['./ride-closed.component.scss'],
})
export class RideClosedComponent implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {}

  onCancel() {
    this.modalController.dismiss();
  }

  onSuccess() {
    this.modalController.create({
      component: ChargingComponent
    }).then(modalEl => {
      modalEl.present();
    });
  }

}
