import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RideClosedComponent } from './ride-closed.component';

describe('RideClosedComponent', () => {
  let component: RideClosedComponent;
  let fixture: ComponentFixture<RideClosedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RideClosedComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RideClosedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
