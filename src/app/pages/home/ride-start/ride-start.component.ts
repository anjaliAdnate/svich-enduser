import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RideClosedComponent } from '../ride-closed/ride-closed.component';

@Component({
  selector: 'ride-start',
  templateUrl: './ride-start.component.html',
  styleUrls: ['./ride-start.component.scss'],
})
export class RideStartComponent implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() { }

  onCancel() {
    this.modalController.dismiss();
  }

  onRideClose() {
    this.modalController.create({
      component: RideClosedComponent
    }).then(modalEl => {
      modalEl.present();
    })
  }

}
