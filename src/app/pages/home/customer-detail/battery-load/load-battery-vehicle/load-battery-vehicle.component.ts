import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RideStartComponent } from '../../../ride-start/ride-start.component';

@Component({
  selector: 'load-battery-vehicle',
  templateUrl: './load-battery-vehicle.component.html',
  styleUrls: ['./load-battery-vehicle.component.scss'],
})
export class LoadBatteryVehicleComponent implements OnInit {

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  onCancel() {
    this.modalController.dismiss();
  }

  onBattery() {
    this.modalController.create({
      component: RideStartComponent
    }).then(modalEL => {
      modalEL.present();
    });
  }

}
