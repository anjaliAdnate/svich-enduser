import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoadBatteryVehicleComponent } from './load-battery-vehicle/load-battery-vehicle.component';
import { HandleErrorComponent } from '../../handle-error/handle-error.component';

@Component({
  selector: 'battery-load',
  templateUrl: './battery-load.component.html',
  styleUrls: ['./battery-load.component.scss'],
})
export class BatteryLoadComponent implements OnInit {
  scanning: boolean;

  constructor(
    private modalController: ModalController,
    // private qrScanner: QRScanner,
    // private toastCtrl: ToastController,
  ) { }

  ngOnInit() {
    // this.onClicked();
  }

  onCancel() {
    this.modalController.dismiss();
  }

  onClicked(key) {
    if (key === 'ok') {
      this.modalController.create({
        component: LoadBatteryVehicleComponent
      }).then(modalEl => {
        modalEl.present();
      });
    } else {
      this.modalController.create({
        component: HandleErrorComponent
      }).then(modalEl => {
        modalEl.present();
      });
    }

  }

  // showCamera() {
  //   (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
  // }

  // hideCamera() {
  //   (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
  //   let content = <HTMLElement>document.getElementsByTagName("body")[0];
  //   content.style.background = "white !important";
  // }

  ionViewDidEnter() {

    // this.qrScanner.prepare()
    //   .then((status: QRScannerStatus) => {
    //     if (status.authorized) {
    //       // camera permission was granted

    //       this.toastCtrl.create({
    //         message: 'camera permission granted',
    //         duration: 1000
    //       }).then(toatEl => {
    //         toatEl.present();
    //       });

    //       // start scanning

    //       this.qrScanner.show()
    //       this.showCamera();
    //       // window.document.querySelector('ion-app').classList.add('cameraView');

    //       let scanSub = this.qrScanner.scan().subscribe((text: string) => {

    //         console.log('Scanned something', text);


    //         this.qrScanner.hide(); // hide camera preview
    //         this.hideCamera();
    //         // window.document.querySelector('ion-app').classList.remove('cameraView');

    //         this.toastCtrl.create({
    //           message: 'You scanned text is this :' + text,
    //           duration: 6000
    //         }).then(toatEl => {
    //           toatEl.present();
    //         });
    //         scanSub.unsubscribe(); // stop scanning
    //       });


    //     } else if (status.denied) {
    //       this.toastCtrl.create({
    //         message: 'camera permission was denied',
    //         duration: 3000
    //       }).then(toatEl => {
    //         toatEl.present();
    //       });
    //       // camera permission was permanently denied
    //       // you must use QRScanner.openSettings() method to guide the user to the settings page
    //       // then they can grant the permission from there
    //     } else {
    //       this.toastCtrl.create({
    //         message: 'You can ask for permission again at a later time.',
    //         duration: 3000
    //       }).then(toatEl => {
    //         toatEl.present();
    //       });
    //       // permission was denied, but not permanently. You can ask for permission again at a later time.
    //     }
    //   })
    //   .catch((e: any) => console.log('Error is', e));

  }

  // showCamera() {
  //   (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
  // }
  // hideCamera() {
  //   (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
  // }

  ionViewWillLeave() {

    // window.document.querySelector('ion-app').classList.remove('cameraView');

  }

}
