import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BatteryLoadComponent } from './battery-load/battery-load.component';

@Component({
  selector: 'customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss'],
})
export class CustomerDetailComponent implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() { }

  onCancel() {
    this.modalController.dismiss();
  }

  onProcess() {
    this.modalController.create({
      component: BatteryLoadComponent
    }).then(modalEl => {
      modalEl.present();
    });
  }

}
