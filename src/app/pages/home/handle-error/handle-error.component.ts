import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'handle-error',
  templateUrl: './handle-error.component.html',
  styleUrls: ['./handle-error.component.scss'],
})
export class HandleErrorComponent implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() { }

  onCancel() {
    this.modalController.dismiss();
  }
}
