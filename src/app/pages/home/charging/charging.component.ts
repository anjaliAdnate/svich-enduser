import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ThankYouComponent } from '../thank-you/thank-you.component';

@Component({
  selector: 'charging',
  templateUrl: './charging.component.html',
  styleUrls: ['./charging.component.scss'],
})
export class ChargingComponent implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {}

  onCancel() {
    this.modalController.dismiss();
  }

  onClicked() {
    this.modalController.create({
      component: ThankYouComponent
    }).then(modalEl => {
      modalEl.present();
    })
  }

}
