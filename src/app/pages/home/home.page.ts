import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';

@Component({
  selector: 'home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }

  onClicked() {
    this.modalController.create({
      component: CustomerDetailComponent
    }).then(modalEl => {
      modalEl.present();
    });
  }

}
